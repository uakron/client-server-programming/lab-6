var _ = require('lodash');
var fs = require('fs');
var express = require('express');
var app = express();
var port = 8080;

app.get('/customers', function(req, res) {
    fs.readFile(__dirname + "/" + "customers.json", 'utf8', function(err, data) {
        console.log(data);
        if (err) throw err;
        var json_data = JSON.parse(data);
        res.write("<table>");
        for (var customer in json_data.customers) {
            res.write("<tr>" + JSON.stringify(json_data["customers"][customer]) + "</tr>" + "<br><br>");
        }
        res.write("</table>");
    });
});

app.get('/customers/:uid', function(req, res) {
    fs.readFile(__dirname + "/" + "customers.json", 'utf8', function(err, data) {
        if (err) throw err;
        var json_data = JSON.parse(data);
        var user = _.filter(json_data.customers, {id: Number(req.params.uid) });
        res.write("<div>" + JSON.stringify(user) + "</div>");
    });
});

console.log("Page is active on 8080!");
app.listen(port);